package dz.blida.exercise.controllers;

import dz.blida.exercise.dtos.MessageResponseDto;
import dz.blida.exercise.functions.Global;
import dz.blida.exercise.services.family.FamilyDto;
import dz.blida.exercise.services.family.FamilyService;
import dz.blida.exercise.services.excelFile.ExcelFileService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/families")
@AllArgsConstructor
public class FamilyController {

    private final FamilyService familyService;

    private final ExcelFileService excelFileService;

    @GetMapping("/{id}")
    public FamilyDto read(@PathVariable("id") Long id) {
        return familyService.findById(id);
    }

    @GetMapping()
    public List<FamilyDto> readAll() {
        return familyService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public FamilyDto store(@Valid @ModelAttribute FamilyDto familyDto) {
        return familyService.save(familyDto);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public FamilyDto update(@PathVariable("id") Long id,
                            @Valid @ModelAttribute FamilyDto familyDto) {

        FamilyDto currentFamily = familyService.findById(id);
        BeanUtils.copyProperties(familyDto, currentFamily, Global.getNullPropertyNames(familyDto));

        return familyService.save(currentFamily);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        familyService.delete(id);
        return ResponseEntity.ok(new MessageResponseDto("Book Family Deleted successfully!"));
    }

    @GetMapping("/export/excel")
    public void exportFamilies(HttpServletResponse response) throws IOException {
        excelFileService.export(
                response,
                familyService.getFamilyColumnsAsStringList(),
                familyService.getFamiliesAsStringList());
    }
}