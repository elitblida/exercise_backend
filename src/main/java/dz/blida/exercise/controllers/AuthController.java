package dz.blida.exercise.controllers;

import dz.blida.exercise.dtos.JwtResponseDto;
import dz.blida.exercise.dtos.MessageResponseDto;
import dz.blida.exercise.security.jwt.JwtUtils;
import dz.blida.exercise.services.user.UserLoginDto;
import dz.blida.exercise.services.user.UserService;
import dz.blida.exercise.services.user.UserSignupDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthController {

    AuthenticationManager authenticationManager;

    UserService userService;

    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserLoginDto userLogin) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userLogin.getUsername(), userLogin.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        return ResponseEntity.ok(new JwtResponseDto(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserSignupDto userSignupDto) {
        System.out.println("ddd");
        userService.save(userSignupDto);
        return ResponseEntity.ok(new MessageResponseDto("User registered successfully!"));
    }
}
