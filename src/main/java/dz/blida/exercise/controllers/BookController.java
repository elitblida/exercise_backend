package dz.blida.exercise.controllers;

import dz.blida.exercise.dtos.MessageResponseDto;
import dz.blida.exercise.services.book.BookDto;
import dz.blida.exercise.services.book.BookService;
import dz.blida.exercise.services.family.FamilyService;
import dz.blida.exercise.services.excelFile.ExcelFileService;
import dz.blida.exercise.services.fileStorage.FileStorageServiceImp;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/books")
@AllArgsConstructor
public class BookController {

    private final BookService bookService;

    private final FamilyService familyService;

    private final FileStorageServiceImp fileStorageService;

    private final ExcelFileService excelFileService;

    @GetMapping("/{id}")
    public BookDto read(@PathVariable("id") Long id) {
        return bookService.findById(id);
    }

    @GetMapping()
    public List<BookDto> readAll() {
        return bookService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public BookDto store(@Valid @ModelAttribute BookDto bookDto,
                         @RequestParam(value = "image", required = false) MultipartFile file,
                         @RequestParam(value = "selectedFamilies", defaultValue = "", required = false) List<Long> bookFamilies) {

        bookDto.setFamilies(familyService.findByIdIn(bookFamilies));

        if (file != null && !file.isEmpty()) {
            String fileName = fileStorageService.storeFile(file);
            bookDto.setImagePath(fileName);
        }
        return bookService.save(bookDto);
    }

    @PutMapping(value = "{id}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public BookDto update(@PathVariable("id") Long id,
                          @Valid @ModelAttribute BookDto bookDto,
                          @RequestParam(value = "image", required = false) MultipartFile file,
                          @RequestParam(value = "selectedFamilies", defaultValue = "", required = false) List<Long> bookFamilies
    ) {
        bookDto.setFamilies(familyService.findByIdIn(bookFamilies));
        BookDto currentBook = bookService.findById(id);
        BeanUtils.copyProperties(bookDto, currentBook, "id", "imagePath");

        if (file != null && !file.isEmpty()) {
            String fileName = fileStorageService.storeFile(file);
            currentBook.setImagePath(fileName);
        }
        return bookService.save(currentBook);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        bookService.delete(id);
        return ResponseEntity.ok(new MessageResponseDto("Book Deleted successfully!"));
    }

    @GetMapping("/export/excel")
    public void exportBooks(HttpServletResponse response) throws IOException {
        excelFileService.export(
                                response,
                                bookService.getBookColumnsAsStringList(),
                                bookService.getBooksAsStringList());
    }

}
