package dz.blida.exercise.configuration;

import dz.blida.exercise.models.*;
import dz.blida.exercise.repostirories.FamilyRepository;
import dz.blida.exercise.repostirories.BookRepository;
import dz.blida.exercise.repostirories.RoleRepository;
import dz.blida.exercise.repostirories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@AllArgsConstructor
public class DataLoader implements ApplicationRunner {

    private final BookRepository bookRepository;

    private final FamilyRepository familyRepository;

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    PasswordEncoder encoder;

    @Override
    public void run(ApplicationArguments args) {
        initBookAndBookFamily();
        initUserAndRole();

    }

    private void initUserAndRole(){

        Role role1 = new Role("USER");
        roleRepository.saveAndFlush(role1);
        Role role2 = new Role("ADMIN");
        roleRepository.saveAndFlush(role2);
        Role role3 = new Role("MODERATOR");
        roleRepository.saveAndFlush(role3);

        Set<Role> roles = new HashSet<>();

        User user1 = new User("othmane","othmane@gmail.com",   encoder.encode("1"));
        roles.add(role1);
        user1.setRoles(roles);
        userRepository.saveAndFlush(user1);

        User user2 = new User("othmane2","othmane2@gmail.com",encoder.encode("2"));
        roles.add(role2);
        user2.setRoles(roles);
        userRepository.saveAndFlush(user2);

        User user3 = new User("othmane3","othmane3@gmail.com",encoder.encode("3"));
        roles.add(role3);
        user3.setRoles(roles);
        userRepository.saveAndFlush(user3);
    }

    private void initBookAndBookFamily(){
        Book book1 = new Book();
        book1.setName("the lord of the rings");
        book1.setAuthor("John Ronald Reuel Tolkien");
        book1.setCopyright("some copyright");
        book1.setDescription( "some description");
        book1.setImagePath("the lord of the rings.jpg");
        bookRepository.saveAndFlush(book1);
        Book book2 = new Book();

        book2.setName("hobbits");
        book2.setAuthor("John Ronald Reuel Tolkien");
        book2.setCopyright("some copyright");
        book2.setDescription( "some description");
        book2.setImagePath("hobbit.jpg");
        bookRepository.saveAndFlush(book2);

        Book book3 = new Book();
        book3.setName("harry potter ");
        book3.setAuthor("J.K. Rowling");
        book3.setCopyright("some copyright");
        book3.setDescription( "some description");
        book3.setImagePath("harry potter.jpg");
        bookRepository.saveAndFlush(book3);


        List<Book> bookList = new ArrayList<>() ;
        bookList.add(book1);

        Family family = new Family();
        family.setName("Action");
        family.setDescription("some Action Description");
        family.setBooks(bookList);
        familyRepository.saveAndFlush(family);

        bookList.add(book2);

        Family family1 = new Family();
        family1.setName("Drama");
        family1.setDescription("some Drama Description");
        family1.setBooks(bookList);
        familyRepository.saveAndFlush(family1);

        bookList.add(book3);
        Family family2 = new Family();
        family2.setName("Thriller");
        family2.setDescription("some Thriller Description");
        family2.setBooks(bookList);
        familyRepository.saveAndFlush(family2);
    }


}