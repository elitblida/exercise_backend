package dz.blida.exercise.services.book;

import java.util.List;
public interface BookService {

    BookDto findById(Long bookId);

    List<BookDto> findAll();

    List<BookDto> findByIdIn(List<Long> Ids);

    BookDto save(BookDto bookDto);

    void delete(Long bookId);

    List<List<String>> getBooksAsStringList();

    List<String> convertBookToStringList(BookDto bookDto);

    List<String> getBookColumnsAsStringList();
}
