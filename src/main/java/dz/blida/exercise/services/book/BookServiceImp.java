package dz.blida.exercise.services.book;

import dz.blida.exercise.exeptions.ApiRequestException;
import dz.blida.exercise.models.Book;
import dz.blida.exercise.models.Family;
import dz.blida.exercise.repostirories.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BookServiceImp implements BookService {

    public final BookRepository bookRepository;

    public final BookMapper bookMapper;

    @Override
    public BookDto findById(Long bookId) {
        if (!bookRepository.existsById(bookId))
            throw new ApiRequestException( String.format("Book with id %d not found", bookId));
        Book book =  bookRepository.findById(bookId).get();
        return bookMapper.toBookDto(book);
    }

    @Override
    public List<BookDto> findAll() {
        List<Book> books =  bookRepository.findAll();
        return bookMapper.BooksToBooksDtos(books);
    }

    @Override
    public List<BookDto> findByIdIn(List<Long> Ids) {
        List<Book> books =  bookRepository.findByIdIn(Ids);
        return bookMapper.BooksToBooksDtos(books);
    }

    @Override
    public BookDto save(BookDto bookDto) {
        Book book = bookMapper.toBook(bookDto);
        return bookMapper.toBookDto(bookRepository.save(book));
    }

    @Override
    public void delete(Long bookId) {
        try {
            bookRepository.deleteById(bookId);
        } catch (EmptyResultDataAccessException ignored) {
            throw new ApiRequestException(String.format("Book  with id %d not deleted check with admin", bookId));

        }
    }

    @Override
    public List<List<String>> getBooksAsStringList() {
         return findAll()
                .stream()
                .map(this::convertBookToStringList)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> convertBookToStringList(BookDto bookDto) {
        List<String> row = new ArrayList<>();
        row.add(bookDto.getName());
        row.add(bookDto.getAuthor());
        row.add(bookDto.getCopyright());
        row.add(bookDto.getDescription());
        bookDto.getFamilies().forEach(family -> {
            row.add(family.getName());
        });
        return row;
    }

    @Override
    public List<String> getBookColumnsAsStringList() {
        List<String> header = new ArrayList<>();
        Long BiggestNumberOfFamiliesByBook = bookRepository.getBiggestNumberOfFamiliesByBook();
        header.add("Name");
        header.add("Author");
        header.add("CopyRight");
        header.add("Description");
        if(BiggestNumberOfFamiliesByBook != null && BiggestNumberOfFamiliesByBook > 0)
        for (int i = 0; i < bookRepository.getBiggestNumberOfFamiliesByBook(); i++)
            header.add("Book Family Number : " + i);
        return header;
    }



}
