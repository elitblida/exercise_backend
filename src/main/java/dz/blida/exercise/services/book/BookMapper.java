package dz.blida.exercise.services.book;

import dz.blida.exercise.models.Book;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper {
    
    BookDto toBookDto(Book Book);

    Book toBook(BookDto bookDto);

    List<BookDto> BooksToBooksDtos(List<Book> Books);

}
