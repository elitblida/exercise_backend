package dz.blida.exercise.services.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookDto {

    private Long id;
    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String name;
    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String author;
    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String copyright;
    @Size(
            max = 255,
            message = "The  '${validatedValue}' must be smaller than {max} characters long")
    private String description;
    @Size(
            max = 255,
            message = "The  '${validatedValue}' must be smaller than {max} characters long")
    private String imagePath ;
    private List<BookFamiliesDto> families;
}
