package dz.blida.exercise.services.excelFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

public interface ExcelFileService {

	ByteArrayInputStream createExcel(List<String> Header ,List<List<String>> Content ) throws IOException;

	void export(HttpServletResponse response, List<String> Header , List<List<String>> Content) throws IOException;


	
}
