package dz.blida.exercise.services.excelFile;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelFileServiceImpl implements ExcelFileService {

	@Override
	public ByteArrayInputStream createExcel(List<String> header, List<List<String>> content) throws IOException {
		Workbook workbook = new XSSFWorkbook();
			Sheet sheet = workbook.createSheet("exportFile");
			Row row = sheet.createRow(0);

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			for(int i = 0; i < header.size(); i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(header.get(i));
				cell.setCellStyle(headerCellStyle);
				sheet.autoSizeColumn(i);
			}

			for(int i = 0; i < content.size(); i++) {
				Row dataRow = sheet.createRow(i + 1);
				for(int j = 0; j < content.get(i).size(); j++) {
					dataRow.createCell(j).setCellValue(content.get(i).get(j));
				}
			}

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			workbook.write(outputStream);
			return new ByteArrayInputStream(outputStream.toByteArray());
	}

	@Override
	public void export(HttpServletResponse response, List<String> Header, List<List<String>> Content) throws IOException {
		ByteArrayInputStream byteArrayInputStream = createExcel(Header, Content);
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=contacts.xlsx");
		IOUtils.copy(byteArrayInputStream, response.getOutputStream());
	}


}
