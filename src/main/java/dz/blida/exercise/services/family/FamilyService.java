package dz.blida.exercise.services.family;

import dz.blida.exercise.services.book.BookFamiliesDto;

import java.util.List;

public interface FamilyService {

    FamilyDto findById(Long familyId);

    List<FamilyDto> findAll();

    List<BookFamiliesDto> findByIdIn(List<Long> Ids);

    FamilyDto save(FamilyDto familyDto);

    void delete(Long familyId);

    List<List<String>> getFamiliesAsStringList();

    List<String> convertFamilyToStringList(FamilyDto familyDto);

    List<String> getFamilyColumnsAsStringList();
}