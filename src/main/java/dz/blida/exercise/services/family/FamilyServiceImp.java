package dz.blida.exercise.services.family;

import dz.blida.exercise.exeptions.ApiRequestException;
import dz.blida.exercise.models.Family;
import dz.blida.exercise.repostirories.FamilyRepository;
import dz.blida.exercise.services.book.BookDto;
import dz.blida.exercise.services.book.BookFamiliesDto;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FamilyServiceImp implements FamilyService {

    public final FamilyRepository familyRepository;

    public final FamilyMapper familyMapper;

    @Override
    public FamilyDto findById(Long familyId) {
        if (!familyRepository.existsById(familyId))
            throw new ApiRequestException(String.format("Book Family with id %d not found", familyId));
        Family family = familyRepository.findById(familyId).get();
        return familyMapper.toFamilyDto(family);
    }

    @Override
    public List<FamilyDto> findAll() {
        List<Family> families = familyRepository.findAll();
        return familyMapper.FamiliesToFamiliesDtos(families);
    }

    @Override
    public List<BookFamiliesDto> findByIdIn(List<Long> Ids) {
        List<Family> families = familyRepository.findByIdIn(Ids);
        return familyMapper.BookFamiliesToBookFamiliesDtos(families);
    }

    @Override
    public FamilyDto save(FamilyDto familyDto) {
        Family family = familyMapper.toFamily(familyDto);
        return familyMapper.toFamilyDto(familyRepository.save(family));
    }

    @Override
    public void delete(Long familyId) {
        try {
            familyRepository.deleteById(familyId);
        } catch (EmptyResultDataAccessException ignored) {
            throw new ApiRequestException(String.format("Book Family with id %d not deleted check with admin", familyId));
        }
    }

    @Override
    public List<List<String>> getFamiliesAsStringList() {
        return findAll()
                .stream()
                .map(this::convertFamilyToStringList)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> convertFamilyToStringList(FamilyDto familyDto) {
        List<String> row = new ArrayList<>();
        row.add(familyDto.getName());
        row.add(familyDto.getDescription());
        familyDto.getBooks().forEach(book -> {
            row.add(book.getName());
        });
        return row;
    }

    @Override
    public List<String> getFamilyColumnsAsStringList() {
        List<String> header = new ArrayList<>();
        Long BiggestNumberOfBooksByFamily =  familyRepository.getBiggestNumberOfBooksByFamily();
        header.add("Name");
        header.add("Description");
        if(BiggestNumberOfBooksByFamily!= null && BiggestNumberOfBooksByFamily> 0)
        for (int i = 0; i < familyRepository.getBiggestNumberOfBooksByFamily(); i++)
            header.add("Book Number : " + i);
        return header;
    }
}
