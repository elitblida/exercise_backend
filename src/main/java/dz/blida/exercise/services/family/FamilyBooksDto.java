package dz.blida.exercise.services.family;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FamilyBooksDto {

    private Long id;
    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String name;
    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String author;
    @Size(min = 2,
            max = 50,
            message = "The  '${validatedValue}' must be between {min} and {max} characters long")
    private String copyright;
    @Size( max = 255,
            message = "The  '${validatedValue}' must be smaller than {max} characters long")
    private String description;
    @Size(
            max = 255,
            message = "The  '${validatedValue}' must be smaller than {max} characters long")
    private String imagePath ;

}
