package dz.blida.exercise.services.family;

import dz.blida.exercise.models.Family;
import dz.blida.exercise.services.book.BookFamiliesDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FamilyMapper {
    
    FamilyDto toFamilyDto(Family family);

    Family toFamily(FamilyDto familyDto);

    List<FamilyDto> FamiliesToFamiliesDtos(List<Family> families);

    List<BookFamiliesDto> BookFamiliesToBookFamiliesDtos(List<Family> families);



}
