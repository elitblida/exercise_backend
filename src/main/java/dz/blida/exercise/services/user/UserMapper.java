package dz.blida.exercise.services.user;

import dz.blida.exercise.models.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {



  UserDto toUserDto(User user);

  List<UserDto> usersToUsersDtos(List<User> users);

  User toUser(UserDto userDto);

  User userSingInToUser(UserSignupDto userSignupDto);

}
