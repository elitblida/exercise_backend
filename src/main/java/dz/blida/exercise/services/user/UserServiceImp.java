package dz.blida.exercise.services.user;

import dz.blida.exercise.exeptions.ApiRequestException;
import dz.blida.exercise.models.Role;
import dz.blida.exercise.models.User;
import dz.blida.exercise.repostirories.RoleRepository;
import dz.blida.exercise.repostirories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class UserServiceImp implements UserService {

    UserRepository userRepository;

    RoleRepository roleRepository;

    PasswordEncoder encoder;

    public final UserMapper userMapper;

    @Override
    public UserDto findById(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
        return userMapper.toUserDto(user);
    }

    @Override
    public List<UserDto> findAll() {
        List<User> users = userRepository.findAll();
        return userMapper.usersToUsersDtos(users);
    }

    @Override
    public UserDto save(UserSignupDto userSignupDto) {
        if (userExist(userSignupDto))
            throw new ApiRequestException("user already exist");

        if (!roleExist(userSignupDto.getRole()))
            throw new ApiRequestException("role not exist");

        User user = userMapper.userSingInToUser(userSignupDto);
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByName(userSignupDto.getRole()));

        user.setPassword(encoder.encode(user.getPassword()));
        user.setRoles(roles);

        return userMapper.toUserDto(userRepository.save(user));
    }

    private Boolean userExist(UserSignupDto userSignupDto) {
        if(userRepository.existsByUsername(userSignupDto.getUsername()) ||
                userRepository.existsByEmail(userSignupDto.getEmail()))
            return true;
        return false;
    }

    private Boolean roleExist(String role) {
        return roleRepository.existsByName(role);
    }
}
