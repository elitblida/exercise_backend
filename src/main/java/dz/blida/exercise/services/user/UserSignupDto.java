package dz.blida.exercise.services.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserSignupDto {

  @NotBlank
  @Size(max = 20)
  private String username;

  @NotBlank
  @Size(min = 5,message = "this email mast be at least 5 characters")
  @Email(message = "this email not valid")
  private String email;


  @Size(max = 50)
  private String role;

  @NotBlank
  @Size(max = 120)
  private String password;

}
