package dz.blida.exercise.services.user;


import java.util.List;

public interface UserService {

    UserDto findById(Long userId);

    List<UserDto> findAll();

    UserDto save(UserSignupDto userSignupDto);

}
