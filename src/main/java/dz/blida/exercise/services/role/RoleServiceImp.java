package dz.blida.exercise.services.role;

import dz.blida.exercise.models.Role;
import dz.blida.exercise.repostirories.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RoleServiceImp implements RoleService {

    RoleRepository roleRepository;

    public final RoleMapper RoleMapper;
    @Override
    public RoleDto findById(Long RoleId) {
        Role Role =  roleRepository.findById(RoleId).orElseThrow(() -> new RuntimeException("Role not found"));
        return RoleMapper.toRoleDto(Role);
    }

    @Override
    public List<RoleDto> findAll() {
        List<Role> Roles =  roleRepository.findAll();
        return RoleMapper.RolesToRolesDtos(Roles);
    }

    @Override
    public RoleDto save(RoleDto RoleDto) {
        Role Role = RoleMapper.toRole(RoleDto);
        return RoleMapper.toRoleDto(roleRepository.save(Role));
    }
}
