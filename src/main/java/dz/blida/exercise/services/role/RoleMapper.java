package dz.blida.exercise.services.role;

import dz.blida.exercise.models.Role;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleMapper {

  RoleDto toRoleDto(Role Role);

  List<RoleDto> RolesToRolesDtos(List<Role> Roles);

  Role toRole(RoleDto RoleDto);

}
