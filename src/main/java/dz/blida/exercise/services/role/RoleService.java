package dz.blida.exercise.services.role;


import java.util.List;

public interface RoleService {

    RoleDto findById(Long RoleId);

    List<RoleDto> findAll();

    RoleDto save(RoleDto RoleDto);

}
