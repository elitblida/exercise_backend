package dz.blida.exercise.services.fileStorage;

import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
     String storeFile(MultipartFile file);
}
