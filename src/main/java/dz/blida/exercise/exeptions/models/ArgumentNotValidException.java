package dz.blida.exercise.exeptions.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Getter
public class ArgumentNotValidException {
    public final Map<String, List< String>> errors;
    public final HttpStatus httpStatus;
    public final ZonedDateTime zonedDateTime;
}
