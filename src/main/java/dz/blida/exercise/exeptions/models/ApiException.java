package dz.blida.exercise.exeptions.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;
@AllArgsConstructor
@Getter
public class ApiException {
    public final String message;
    public final HttpStatus httpStatus;
    public final ZonedDateTime zonedDateTime;

}
