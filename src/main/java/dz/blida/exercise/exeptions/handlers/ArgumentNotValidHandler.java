package dz.blida.exercise.exeptions.handlers;

import dz.blida.exercise.exeptions.models.ArgumentNotValidException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class ArgumentNotValidHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleValidationExceptions(MethodArgumentNotValidException ex) {
        ArgumentNotValidException argumentNotValidException = getEntityErrors(ex);
        return new ResponseEntity<>(argumentNotValidException,argumentNotValidException.httpStatus);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public ResponseEntity<Object>
    test(BindException ex) {
        ArgumentNotValidException argumentNotValidException = getEntityErrors(ex);
        return new ResponseEntity<>(argumentNotValidException,argumentNotValidException.httpStatus);
    }

    private ArgumentNotValidException getEntityErrors(BindException ex ){
        Map<String, List< String>> entityError = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> {
            List<String> fieldErrors = new ArrayList<>();
            if(entityError.containsKey(error.getField()))
                fieldErrors = entityError.get(error.getField());
            fieldErrors.add(error.getDefaultMessage());
            entityError.put(error.getField(),fieldErrors);
        });
        return new ArgumentNotValidException(
                entityError,
                HttpStatus.BAD_REQUEST,
                ZonedDateTime.now()
        );
    }
}
