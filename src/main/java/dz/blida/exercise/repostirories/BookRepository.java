package dz.blida.exercise.repostirories;

import dz.blida.exercise.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRepository extends JpaRepository<Book,Long> {
    @Query(nativeQuery = true,
            value = "select count(*) \n" +
                    "from books_families \n" +
                    "group by book_id\n" +
                    "order by count(*) desc\n" +
                    "limit 1")
    public Long getBiggestNumberOfFamiliesByBook();

    List<Book> findByIdIn(List<Long> ids);

    boolean existsById(Long id);
}
