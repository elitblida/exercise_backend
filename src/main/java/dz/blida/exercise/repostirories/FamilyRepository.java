package dz.blida.exercise.repostirories;

import dz.blida.exercise.models.Family;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FamilyRepository extends JpaRepository<Family,Long> {

    @Query(nativeQuery = true,
            value = "select count(*) \n" +
                    "from books_families \n" +
                    "group by family_id\n" +
                    "order by count(*) desc\n" +
                    "limit 1")
    public Long getBiggestNumberOfBooksByFamily();

    List<Family> findByIdIn(List<Long> ids);

    boolean existsById(Long id);
}
