### Exercice
******
- This is an application that gives you the ability to manage (CRUD) books with book families ,  this application is fully secured with jwt authentication from spring security


### Installation dependencies
******

- JAVA 11
- maven

### Some dependencies chosen

- H2 :  database source
- Swagger :  Documentation
- Apache Poi : excel exportation
- Lombok : Cleaner and esier for devlopper
- mapstruct : For mapping

### Installation Note
there is property that has named <abbr >file.upload-dir</abbr> in <abbr >application.properties</abbr>  
which u should be updated to where u will put your frontend application.
- I give you an example in the comment section near file.upload-dir 
- this option is for uploading book image in the frontend application for easier handling their locations

#### Documentation
******
When the app is fully running, you can go to this link
http://localhost:8088/swagger-ui/index.html to  obtain the API documentation provided by Swagger.

#### Docker
******
to run this application as container in docker u should
- go to backend project in the terminal
- create your jar file
- exec
- `docker build -t exercise-back .`
- ` docker run -p 8088:8088 exercise-back`

#### PostMan Collections
******
- click on that import button in the top bar
- choose link option
- paste this link  https://www.postman.com/collections/b64833d1636c22b681a2

#### Frontend Application
******
- go visit : https://gitlab.com/elitblida/exercice_frontend



